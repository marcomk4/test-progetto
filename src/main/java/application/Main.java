package application;

import controllers.core.GameEngine;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * ARKANOID GAME.
 * 
 *
 */
public class Main extends Application {
    private static final double PROPORTION_WIDTH = 1.5;
    private static final double PROPORTION_HEIGHT = 1.1;
    private double screenWidth;
    private double screenHeight;

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(final Stage primaryStage) throws Exception {
        Rectangle2D screenRes = Screen.getPrimary().getBounds();
        this.screenWidth = screenRes.getWidth() / PROPORTION_WIDTH;
        this.screenHeight = screenRes.getHeight() / PROPORTION_HEIGHT;
        final GameEngine engine = new GameEngine();
        engine.setup(this, screenWidth, screenHeight);
        engine.setDaemon(true);
        engine.start();
    }
/**
 * Arkanoid Menu.
 * @param args
 *      menu name
 */
    public static void main(final String[] args) {
        launch(args);
    }
}
