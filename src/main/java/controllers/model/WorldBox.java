package controllers.model;

public class WorldBox extends GameObject{

    private final Pos2d botLeftPos;
    private final Pos2d topLeftPos;
    private final Pos2d topRightPos;
    private final Pos2d botRightPos;
    
    public WorldBox(final Pos2d center, final double edge) {
        super(center, new Speed2d(0,0), 0, edge);
        
        this.botLeftPos = new Pos2d(this.getCurrentPos().getX() - this.getEdge(),
                this.getCurrentPos().getY() + this.getEdge());
        this.topLeftPos = new Pos2d(this.getCurrentPos().getX() - this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge());
        this.topRightPos = new Pos2d(this.getCurrentPos().getX() + this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge());
        this.botRightPos = new Pos2d(this.getCurrentPos().getX() + this.getEdge(),
                this.getCurrentPos().getY() + this.getEdge());
    }


    public Pos2d getBotLeftPos() {
        return this.botLeftPos;
    }


    public Pos2d getTopLeftPos() {
        return this.topLeftPos;
    }


    public Pos2d getTopRightPos() {
        return this.topRightPos;
    }


    public Pos2d getBotRightPos() {
        return this.botRightPos;
    }

}
