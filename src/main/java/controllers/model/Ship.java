package controllers.model;

public class Ship extends GameObject{

    public Ship(final Pos2d center, final double edge) {
        super(center, new Speed2d(0,0),0, edge);
    }


    public Pos2d getLeftPos() {
        return new Pos2d(this.getCurrentPos().getX() - this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge() / 6);
    }


    public Pos2d getRightPos() {
        return new Pos2d(this.getCurrentPos().getX() + this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge() / 6);
    }

}
