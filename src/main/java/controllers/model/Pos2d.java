package controllers.model;


public class Pos2d {

    private double x,y;

    public Pos2d(double x,double y){
        this.x=x;
        this.y=y;
    }

    public Pos2d sum(Speed2d s){
        return new Pos2d(x+s.getX(),y+s.getY());
    }

    public String toString(){
        return "P2d("+x+","+y+")";
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

}
