package controllers.model;

import java.util.ArrayList;
import java.util.List;

import controllers.model.Brick.DirCol;

public class World {

    private Ball ball;
    private Ship ship;
    private final List<Brick> bricks;
    private final WorldBox worldBox;
    private int lives = 4;
    
    public World(final WorldBox worldBox){
        this.bricks = new ArrayList<>();
        this.worldBox = worldBox;
    }
    
    public Ball getBall(){
        return this.ball;
    }
    
    public Ship getShip() {
        return this.ship;
    }

    public void setBall(Ball ba){
        this.ball = ba;
    }
    
    public void setShip(Ship ship){
        this.ship = ship;
    }
    
    public void addBrick(Brick br) {
        this.bricks.add(br);
    }
    
    public void updateState(int delta){
        bricks.stream().forEach(br -> br.updateState(delta));
        ball.updateState(delta);
        ship.updateState(delta);
        checkBoundaries();
        checkCollisions();
        checkShip();
    }

    private void checkBoundaries(){

        final double wTop = worldBox.getTopLeftPos().getY();
        final double wBot = worldBox.getBotLeftPos().getY();
        final double wRight = worldBox.getBotRightPos().getX();
        final double wLeft = worldBox.getBotLeftPos().getX();

        final Pos2d bCurrentPos = ball.getCurrentPos();
        final double bEdge = ball.getEdge();
        final double ballTop = ball.getTopLeftPos().getY();
        final double ballBot = ball.getBotLeftPos().getY();
        final double ballRight = ball.getBotRightPos().getX();
        final double ballLeft = ball.getBotLeftPos().getX();
        
        final Pos2d shipCurrentPos = ship.getCurrentPos();
        final double shipLeft = ship.getLeftPos().getX();
        final double shipRight = ship.getRightPos().getX();
        final double shipEdge = ship.getEdge();
 
        if (ballTop < wTop) {
            ball.setPos(new Pos2d(bCurrentPos.getX(), wTop + bEdge));
            ball.flipSpeedOnY();
        } else if (ballBot > wBot) {
            ball.setPos(new Pos2d(bCurrentPos.getX(), wBot - bEdge));
            ball.flipSpeedOnY();
            this.lives--;
            if (lives == 0) {
                System.out.println("HAI PERSO!!!");
                System.exit(1);
            } else {
                System.out.println("HAI PERSO UNA VITA, TE NE RIMANGONO " + this.lives);
            }

        }

        if (ballRight > wRight) {
            ball.setPos(new Pos2d(wRight - bEdge, bCurrentPos.getY()));
            ball.flipSpeedOnX();
        } else if (ballLeft < wLeft) {
            ball.setPos(new Pos2d(wLeft + bEdge, bCurrentPos.getY()));
            ball.flipSpeedOnX();
        }
        if (shipLeft < wLeft) {
            ship.setPos(new Pos2d(wLeft + ship.getEdge(), shipCurrentPos.getY()));
        } else if (shipRight > wRight) {
            ship.setPos(new Pos2d(wRight - ship.getEdge(), shipCurrentPos.getY()));
        }
    }

    private void checkShip() {
        
        final Pos2d bCurrentPos = ball.getCurrentPos();
        final double bEdge = ball.getEdge();
        final double ballBot = ball.getBotLeftPos().getY();
        
        final double shipEdge = ship.getEdge();
        final double shipCenter = ship.getCurrentPos().getX();
        final double shipTop = ship.getLeftPos().getY();
        final double shipLeft = ship.getLeftPos().getX();
        final double shipRight = ship.getRightPos().getX();
        
        if (ballBot > shipTop) {
            if (bCurrentPos.getX() > shipLeft + shipEdge / 3 && bCurrentPos.getX() < shipCenter) {
                ball.setPos(new Pos2d(bCurrentPos.getX(), shipTop - bEdge));
                ball.setSpeed(new Speed2d(-1,-2));
            } else if (bCurrentPos.getX() < shipRight - shipEdge / 3 && bCurrentPos.getX() >= shipCenter) {
                ball.setPos(new Pos2d(bCurrentPos.getX(), shipTop - bEdge));
                ball.setSpeed(new Speed2d(1,-2));
            } else if (bCurrentPos.getX() >= shipLeft && bCurrentPos.getX() <= shipLeft + shipEdge / 3) {
                ball.setPos(new Pos2d(bCurrentPos.getX(), shipTop - bEdge));
                ball.setSpeed(new Speed2d(-2,-0.75));
            } else if (bCurrentPos.getX() <= shipRight && bCurrentPos.getX() >= shipRight - shipEdge / 3) {
                ball.setPos(new Pos2d(bCurrentPos.getX(), shipTop - bEdge));
                ball.setSpeed(new Speed2d(2,-0.75));
            }
        }
    }

    private void checkCollisions() {

        final Pos2d bCurrentPos = ball.getCurrentPos();
        final double bEdge = ball.getEdge();
        final double ballTop = ball.getTopLeftPos().getY();
        final double ballBot = ball.getBotLeftPos().getY();
        final double ballRight = ball.getBotRightPos().getX();
        final double ballLeft = ball.getBotLeftPos().getX();

        Brick found = null;
        for (Brick br : bricks) {
            final double brTop = br.getTopLeftPos().getY();
            final double brBot = br.getBotLeftPos().getY();
            final double brRight = br.getBotRightPos().getX();
            final double brLeft = br.getBotLeftPos().getX();

            boolean checkLeft = false;
            boolean checkRight = false;
            boolean checkBot = false;
            boolean checkTop = false;

            if (ballLeft <= brRight) {
                checkLeft = true;
            } else {
                br.setDirCol(DirCol.LEFT);
            }
            if (ballRight >= brLeft) {
                checkRight = true;
            } else {
                br.setDirCol(DirCol.RIGHT);
            }
            if (ballBot >= brTop) {
                checkBot = true;
            } else {
                br.setDirCol(DirCol.BOTTOM);
            }
            if (ballTop <= brBot) {
                checkTop = true;
            } else {
                br.setDirCol(DirCol.TOP);
            }
            if (checkLeft && checkRight && checkBot && checkTop) {
                found = br;

                switch (br.getDirCol()) {
                case LEFT:
                    ball.setPos(new Pos2d(brRight + bEdge, bCurrentPos.getY()));
                    ball.flipSpeedOnX();
                    break;
                case RIGHT:
                    ball.setPos(new Pos2d(brLeft - bEdge, bCurrentPos.getY()));
                    ball.flipSpeedOnX();
                    break;
                case BOTTOM:
                    ball.setPos(new Pos2d(bCurrentPos.getX(), brTop - bEdge));
                    ball.flipSpeedOnY();
                    break;
                case TOP:
                    ball.setPos(new Pos2d(bCurrentPos.getX(), brBot + bEdge));
                    ball.flipSpeedOnY();
                    break;
                default:
                    break;
                }

            }
        }

        if (found != null) {
            bricks.remove(found);
            if (bricks.size() == 0) {
                System.out.println("HAI VINTOOOOOO!!!!");
                System.exit(-1);
            }
        }

    }

    public List<GameObject> getSceneEntities(){
        List<GameObject> entities = new ArrayList<GameObject>();
        entities.add(this.ball);
        entities.add(this.ship);
        entities.addAll(this.bricks);
        return entities;
    }

    public WorldBox getWorldBox(){
        return this.worldBox;
    }
}
