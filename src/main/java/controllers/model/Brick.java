package controllers.model;

public class Brick extends GameObject{
    
    enum DirCol {
        TOP, LEFT, BOTTOM, RIGHT;
    }
    
    private DirCol dirCol;
    private final Pos2d botLeftPos;
    private final Pos2d topLeftPos;
    private final Pos2d topRightPos;
    private final Pos2d botRightPos;
    
    public Brick(final Pos2d center, final double edge) {
        super(center, new Speed2d(0,0), 0, edge);
        this.botLeftPos = new Pos2d(this.getCurrentPos().getX() - this.getEdge(),
                this.getCurrentPos().getY() + this.getEdge()/2);
        this.topLeftPos = new Pos2d(this.getCurrentPos().getX() - this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge()/2);
        this.topRightPos = new Pos2d(this.getCurrentPos().getX() + this.getEdge(),
                this.getCurrentPos().getY() - this.getEdge()/2);
        this.botRightPos = new Pos2d(this.getCurrentPos().getX() + this.getEdge(),
                this.getCurrentPos().getY() + this.getEdge()/2);
        
    }

    public Pos2d getBotLeftPos() {
        return this.botLeftPos;
    }

    public Pos2d getTopLeftPos() {
        return this.topLeftPos;
    }

    public Pos2d getTopRightPos() {
        return this.topRightPos;
    }

    public Pos2d getBotRightPos() {
        return this.botRightPos;
    }
    
    public DirCol getDirCol() {
        return this.dirCol;
    }
    
    public void setDirCol(final DirCol d) {
        this.dirCol = d;
    }

}
