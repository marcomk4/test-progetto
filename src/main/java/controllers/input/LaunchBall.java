package controllers.input;

import java.util.List;

import controllers.model.*;

public class LaunchBall implements Command{

    @Override
    public void execute(World world, int delta) {
        Ball ball = world.getBall();
        if (ball.getCurrentAcc() == 0) {
            ball.setAcc(4);
        }

    }

}
