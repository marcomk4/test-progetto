package controllers.input;

public interface UIController {

    void notifyCommand(Command cmd);
}
