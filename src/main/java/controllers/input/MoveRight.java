package controllers.input;

import java.util.List;

import controllers.model.*;

public class MoveRight implements Command{

    @Override
    public void execute(World world, int delta) {
        Ball ball = world.getBall();
        Ship ship = world.getShip();
        if (ball.getCurrentAcc() != 0) {
            ship.setPos(new Pos2d(ship.getCurrentPos().getX() + 10 * delta * 0.001, ship.getCurrentPos().getY()));
        }
    }
}
