package controllers.core;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import application.Main;
import controllers.graphics.GameStage;
import controllers.input.*;
import controllers.model.*;

public class GameEngine extends Thread implements UIController{
    
    private static final double INITIAL_POSX = -7.5;
    private static final double INITIAL_POSY = -7.5;
    private static final double SPACING_WIDTH = 1.05;
    private static final double SPACING_HEIGHT = 0.68;
    private static final double BRICK_WIDTH = 0.5;
    private long period = 30;
    private World world;
    private GameStage stage;
    private BlockingQueue<Command> cmdQueue; 
    
    public GameEngine() {
        cmdQueue = new ArrayBlockingQueue<Command>(100);
    }
    
    public void setup(final Main stage, final double screenWidth, final double screenHeight) {
        this.world = new World(new WorldBox(new Pos2d(0,0),8));
        this.world.setBall(new Ball(new Pos2d(0,6), new Speed2d(1,-2), 0, 0.15));
        this.world.setShip(new Ship(new Pos2d(0,7.5), 1));
        for (int y = 0; y <= 13; y++) {
            for (int x = 0; x <=y; x++) {
                this.world.addBrick(new Brick(new Pos2d(INITIAL_POSX + SPACING_WIDTH * x, INITIAL_POSY + SPACING_HEIGHT * y), BRICK_WIDTH));
            }
        }
        this.stage = new GameStage(world, screenWidth,screenHeight, 20, 20);
        this.stage.setInputController(this);
    }
    
    public void run() {
        long previousTime = System.currentTimeMillis();
        while(true) {
            long currentTime = System.currentTimeMillis();
            int timeElapsed = (int) (currentTime - previousTime);
            processInput(timeElapsed);
            updateGame(timeElapsed);
            render();
            waitForNextFrame(currentTime);
            previousTime = currentTime;
        }
        
    }
    
    protected void processInput(final int timeElapsed) {
        Command cmd = cmdQueue.poll();
        if (cmd != null){
            cmd.execute(world, timeElapsed);
        }
    }
    
    protected void updateGame(final int timeElapsed) {
        stage.update();
        world.updateState(timeElapsed);
    }
    
    protected void render() {
        stage.render();
    }
    
    public void waitForNextFrame(final long currentTime) {
        long lag = System.currentTimeMillis() - currentTime;
        if (lag < period) {
            try {
                Thread.sleep(period-lag);
            } catch (Exception ex){}
        }
    }

    @Override
    public void notifyCommand(Command cmd) {
        cmdQueue.add(cmd);
        
    }
}


